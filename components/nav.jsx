import Link from 'next/link';
import React from 'react';


const links = [ { href: 'https://github.com/segmentio/create-next-app', label: 'Github' } ].map((link) => {
  link.key = `nav-link-${link.href}-${link.label}`;
  return link;
});

class Nav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    return (
      <nav>
        <ul>
          <li>
            <Link prefetch href="/">
              <a>Home</a>
            </Link>
          </li>
          <li>
            <Link prefetch href="/about">
              <a>About</a>
            </Link>
          </li>
          <li>
            <Link prefetch href="/fetch">
              <a>Fetch</a>
            </Link>
          </li>
          <li>
            <Link prefetch href="/shows">
              <a>Shows</a>
            </Link>
          </li>
          <li>
            <Link prefetch href="/mobx">
              <a>Mobx</a>
            </Link>
          </li>
          <ul>
            {links.map(({ key, href, label }) => (
              <li key={key}>
                <Link href={href}>
                  <a>{label}</a>
                </Link>
              </li>
            ))}
          </ul>
        </ul>
      </nav>
    );
  }
}

export default Nav;
