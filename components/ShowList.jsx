import React, { Component } from 'react';
import ModalBox from '../components/ModalBox';

import {observer} from 'mobx-react';
import showListStore from '../store/showListStore';

class ShowList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shows: this.props.shows,
      currentShowModal: []
    };

    this.filterList = this.filterList.bind(this);
    this.showModal = this.showModal.bind(this);
  }

  filterList(event) {
    showListStore.filterList(event.target.value);
  }

  showModal(showParam) {
    showListStore.showModal(showParam);
  }

  render() {
    return (
      <React.Fragment>
        <input type="text" placeholder="Search" onChange={this.filterList} value={showListStore.term} />
        {this.state.shows.filter(searchingForName(showListStore.term)).map(({ show }) => (
          <div className="list-item" key={show.id} onClick={() => this.showModal(show)}>
            <div>
              {show.name} <div>{show.genres[0]}</div>
            </div>
            <br />
          </div>
        ))}
        <ModalBox/>
      </React.Fragment>
    );
  }
}

function searchingForName(term) {
  return function(x) {
    return x.show.name.toLowerCase().includes(term.toLowerCase()) || !term;
  };
}

export default observer(ShowList);