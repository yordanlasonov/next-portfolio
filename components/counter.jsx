import React from 'react';

import {observer} from 'mobx-react';
import store from '../store/counterStore';

class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this);
  }


  increment(){
    store.increment();
  }

  decrement(){
    store.decrement();
  }

  render() {
    return (
      <div>
        <h2>Counter</h2>
        <div>
          <button onClick={this.decrement}>-</button>
          <span>{store.counter}</span>
          <button onClick={this.increment}>+</button>
        </div>
      </div>
    );
  }
}

export default observer(Counter);