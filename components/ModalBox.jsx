import React, { Component } from 'react';

import {observer} from 'mobx-react';
import showListStore from '../store/showListStore';

class ModalBox extends Component {
  render() {
    return (
      <div className='modal-box'>
        <h1>I am a modal box</h1>
        <h2>{showListStore.show.name}</h2>
        <h2>{showListStore.show.id}</h2>
        <style jsx>
          {
            `
            .modal-box{
              background-color: gray;
              height: 500px;
              width: 500px;
              position: fixed;
              top:20%;
              left: 50%;
            }
            `
          }
        </style>
      </div>
    );
  }
}

export default observer(ModalBox);