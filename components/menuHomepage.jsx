import Link from 'next/link';
import React from 'react';
import style from './menuHomepage.scss';
import menuHomepageStore from '../store/menuHomepageStore';
import {observer} from 'mobx-react';


class MenuHomepage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    menuHomepageStore.onScroll(window.scrollY);
    document.addEventListener('scroll', () => {
      menuHomepageStore.onScroll(window.scrollY);
    });
  }

  render() {
    return (
      <div id='header_nav' style={{left:menuHomepageStore.positionLeft + '%'}} className={style.menuHomepageWrapper}>
        <header>
          <div>
            <img src='../static/logo.svg' height='60'/>
          </div>
        </header>
        <nav>
          <ul>
            <li>
              <a>
                <span>
                  Profile
                </span>
              </a>
            </li>
            <li>
              <a>
                <span>
                  Work
                </span>
              </a>
            </li>
            <li>
              <a>
                <span>
                  Contact
                </span>
              </a>
            </li>
            <li>
              <a>
                <span>
                  Blog
                </span>
              </a>
            </li>
            <li>
              <a>
                <span>
                  LinkedIn
                </span>
              </a>
            </li>
            <li>
              <a>
                <span>
                  GitHub
                </span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default observer(MenuHomepage);
