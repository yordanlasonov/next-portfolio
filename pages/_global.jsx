import React, { Component } from 'react';
import '../styles/normalize.scss';
import '../styles/fonts.scss';

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
  }
  
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}
