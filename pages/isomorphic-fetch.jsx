import 'isomorphic-fetch';

const Page = ({ name, company }) => (
  <div>
    <p>{name}</p>
    <p>{company}</p>
  </div>
);

Page.getInitialProps = () => {
  return fetch('https://api.github.com/users/octocat')
    .then(res => res.json());
};

export default Page;