import React from 'react';
import Head from '../components/head';
import Nav from '../components/nav';
import Global from './_global';
import MenuHompage from '../components/menuHomepage';

const height = {
  height: '4000px'
};

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      count: 10,
      isShown: false
    };
    this.increment = this.increment.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState((prevState) => {
      return {
        isShown: !prevState.isShown
      };
    });
  }

  increment() {
    this.setState((prevState) => {
      return {
        count: prevState.count + 1
      };
    });
  }

  render() {
    return (
      <Global>
        <div style={height}>
          <Head title="Home" />
          <Nav />
          <MenuHompage />
        </div>
      </Global>
    );
  }
}
