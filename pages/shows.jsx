import Global from './_global';
import fetch from 'isomorphic-unfetch';
import ShowList from '../components/ShowList';
import Head from '../components/head';
import Nav from '../components/nav';

import {observer} from 'mobx-react';
import showListStore from '../store/showListStore';


const Shows = (props) => (
  <Global>
    <Head title="Home" />
    <Nav />
    <h1>Batman TV Shows</h1>
    <ShowList shows={props.shows}/>
  </Global>
);

Shows.getInitialProps = async function() {
  const res = await fetch('https://api.tvmaze.com/search/shows?q=batman');
  const data = await res.json();
  showListStore.setShowsData(data);
  return {
    shows: data
  };
};

export default observer(Shows);