import React from 'react';
import Head from '../components/head';
import Nav from '../components/nav';
import Global from './_global';
import { Fetch } from 'react-data-fetching';
import styles from './fetch.scss';
import classNames from 'classnames';

export default class fetchData extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
    };
  }

  render() {
    return (
      <Global>
        <Head title="Home" />
        <Nav />
        <Fetch
          url="https://api.github.com/users/octocat"
          loader={<img src='../static/loading.svg'/>}
          onLoad={() => console.log('Started fetching data...')}
          onFetch={({ data }) => this.setState(() => ({ data: data }))}
          onError={({ error }) => this.reportError(error)}
          timeout={1000000}
        >
          {({ data }) => (
            <div>
              <h1 className={styles.headline}>Username</h1>
              <p>{data.name}</p>
              <p>{data.url}</p>
              <p>{data.type}</p>
            </div>
          )}

        </Fetch>
      </Global>
    );
  }
}
