import {observable, action} from 'mobx';

class CounterStore {
  @observable positionLeft = 50;
  
  @action onScroll(scrollY){
    this.positionLeft = 50 - scrollY * 0.075;
      if(this.positionLeft < 10){
        this.positionLeft = 10;
      }
  }
}

export default new CounterStore();