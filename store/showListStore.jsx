import {observable, action} from 'mobx';

class ShowListStore {
  @observable data = ' ';
  @observable term = 'Batman';
  @observable show = ' ';

  @action setShowsData(data){this.data = data}
  @action filterList(term){this.term = term}
  @action showModal(show){this.show = show}

}

export default new ShowListStore();